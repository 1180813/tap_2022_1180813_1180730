package xml

import scala.xml.{Elem, Node}
import domain.Result
import domain.schedule.DomainObjects.*
import domain.schedule.SimpleTypes.*

object XMLtoDomain:

  def scheduleInfo(node: Node): ScheduleInfo =
    val scheduleInfo = for {
      schedulingPeriods <- schedulingPeriod(node)
      nurses <- nurse(node)
      constraints <- constraint(node)
      preferences <- preference(node)
    } yield ScheduleInfo(schedulePeriods, nurses, constraints, preferences)

  def schedulingPeriod(node: Node): SchedulePeriod =
    val schedulePeriods = for {
      nurseRequirements <- nurseRequirement(node)
      id <- SchedulePeriodID.from((node \ "schedulingPeriods" \ "schedulingPeriod" \ "@id").text.toLong)
      start <- SchedulePeriodStart.from((node \ "schedulingPeriods" \ "schedulingPeriod" \ "@start").text)
      end <- SchedulePeriodEnd.from((node \ "schedulingPeriods" \ "schedulingPeriod" \ "@end").text)
    } yield SchedulePeriod(nurseRequirements, id, start, end)
    schedulePeriods.fold(Left(DomainError.SchedulePeriodID("")), sp => Right(sp))

  def nurseRequirement(node: Node): NurseRequirement =
    val nurseRequirements = for {
      role <- NurseRequirementRole.from((node \ "schedulingPeriods" \ "schedulingPeriod" \ "nurseRequirement" \ "@role").text)
      number <- NurseRequirementNumber.from((node \ "schedulingPeriods" \ "schedulingPeriod" \ "nurseRequirement" \ "@number").text.toInt)
    } yield NurseRequirement(role, number)
    nurseRequirements.fold(Left(DomainError.NurseRequirementRoleCannotBeNull("")), nr => Right(nr))

  def nurse(node: Node): Nurse =
    val nurses = for {
      name <- NurseName.from((node \ "nurses" \ "nurse" \ "@name").text)
      seniority <- NurseSeniority.from((node \ "nurses" \ "nurse" \ "@seniority").text.toInt)
      role <- NurseRole.from((node \ "nurses" \ "nurse" \ "nurseRole" \ "@role").text)
    } yield Nurse(name, seniority, role)
    nurses.fold(Left(DomainError.NurseSeniorityOutOfBounds("")), n => Right(n))

  def constraint(node: Node): Constraint =
    val constraints = for {
      minRestDaysPerWeek <- MinRestDaysPerWeek.from((node \ "constraints" \ "@minRestDaysPerWeek").text.toInt)
      maxShiftsPerDay <- MaxShiftsPerDay.from((node \ "constraints" \ "@maxShiftsPerDay").text.toInt)
    } yield Constraint(minRestDaysPerWeek, maxShiftsPerDay)
    constraints.fold(Left(DomainError.MaxShiftsPerDayError("")), c => Right(c))

  def periodPreference(node: Node): PeriodPreference =
    val periodPreferences = for {
      nurse <- NurseName.from((node \ "preferences" \ "period" \ "@nurse").text)
      period <- PeriodType.from((node \ "preferences" \ "period" \ "@period").text)
      value <- PreferenceValue.from((node \ "preferences" \ "period" \ "@value").text.toInt)
    } yield PeriodPreference(nurse, period, value)
    periodPreferences.fold(Left(DomainError.PreferenceOutOfBounds("")), pp => Right(pp))

  def dayPreference(node: Node): DayPreference =
    val dayPreferences = for {
      nurse <- NurseName.from((node \ "preferences" \ "day" \ "@nurse").text)
      day <- DayType.from((node \ "preferences" \ "day" \ "@day").text.toInt)
      value <- PreferenceValue.from((node \ "preferences" \ "day" \ "@value").text.toInt)
    } yield DayPreference(nurse, day, value)
    dayPreferences.fold(Left(DomainError.DayOutOfBounds("")), dp => Right(dp))

  def preference(node: Node): Preference =
    val preferences = for {
      period <- periodPreference(node)
      day <- dayPreference(node)
    } yield Preference(period, day)