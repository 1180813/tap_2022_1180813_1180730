package domain.schedule

import scala.xml.Elem
import domain.{DomainError, Result}
import xml.XMLtoDomain

import scala.annotation.targetName

object ScheduleMS01 extends Schedule:
  
  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    XMLtoDomain.scheduleInfo(xml) match
      case Left(x) => Left(DomainError.XMLError("XML ERROR"))
      case Right (z) => gerarHorario(xml) match
        case Left(l) => Left(DomainError.IOFileProblem("IOFileProblem"))
        //case Right(r) => exportarXml()

  def gerarHorario(xml: Elem): Unit =
    for(i<-1 to 7){
      println(i)
    }
