package domain.schedule

import domain.{DomainError, Result}

import scala.annotation.targetName

object SimpleTypes:

  opaque type SchedulePeriodID = Long
  object SchedulePeriodID:
    @targetName("fromSchedulePeriodID")
    def from(s: Long): Result[SchedulePeriodID] =
      if(s<=0) Left(DomainError.SchedulePeriodID("Schedule Period ID can not be zero or negative"))
      else Right(s)
  extension (n: SchedulePeriodID)
    @targetName("toSchedulePeriodID")
    def to: Long = n
  
  opaque type SchedulePeriodEnd = String
  object SchedulePeriodEnd:
    @targetName("fromSchedulePeriodEnd")
    def from(s: String): Result[SchedulePeriodEnd] =
      if (s == null) Left(DomainError.SchedulePeriodEndCannotBeNull("The Schedule Period End can not be null"))
      else Right(s)
  extension (n: SchedulePeriodEnd)
    @targetName("toSchedulePeriodEnd")
    def to: String = n

  opaque type SchedulePeriodStart = String
  object SchedulePeriodStart:
    @targetName("fromSchedulePeriodStart")
    def from(s: String): Result[SchedulePeriodStart] =
      if (s == null) Left(DomainError.SchedulePeriodStartCannotBeNull("The Schedule Period Start can not be null"))
      else Right(s)
  extension (n: SchedulePeriodStart)
    @targetName("toSchedulePeriodStart")
    def to: String = n
  
  // Minimum rest days per week attribute
  opaque type MinRestDaysPerWeek = Int
  object MinRestDaysPerWeek:
    @targetName("fromMinRestDaysPerWeek")
    def from(i: Int): Result[MinRestDaysPerWeek] =
      if (i <= 0) Left(DomainError.MinRestDaysPerWeek("The minimum rest days per week can't be less or equal than zero"))
      else Right(i)
  extension (m: MinRestDaysPerWeek)
    @targetName("toMinRestDaysPerWeek")
    def to: Int = m

  // Maximum shifts per day attribute
  opaque type MaxShiftsPerDay = Int
  object MaxShiftsPerDay:
    @targetName("fromMaxShiftsPerDay")
    def from(i: Int): Result[MaxShiftsPerDay] =
      if (i <= 0) Left(DomainError.MaxShiftsPerDayError("The maximum shifts per day can't be less or equal than zero"))
      else Right(i)
  extension (m: MaxShiftsPerDay)
    @targetName("toMaxShiftsPerDay")
    def to: Int = m

  // Nurse Role
  opaque type NurseRole = String
  object NurseRole:
    @targetName("fromNurseRole")
    def from(s: String): Result[NurseRole] =
      if(s==null) Left(DomainError.NurseRoleCannotBeNull("Nurse's role can not be null"))
      else Right(s)
  extension (n: NurseRole)
    @targetName("toNurseRole")
    def to: String = n

  // Nurse Requirement Number attribute
  opaque type NurseRequirementNumber = Int
  object NurseRequirementNumber:
    @targetName("fromNurseRequirementNumber")
    def from(i: Int): Result[NurseRequirementNumber] =
      if (i <= 0) Left(DomainError.NurseRequirementNumber("The minimum of nurses required can't be less or equal than zero"))
      else Right(i)
  extension (m: NurseRequirementNumber)
    @targetName("toNurseRequirementNumber")
    def to: Int = m
    
  // Nurse Requirement role attribute
  opaque type NurseRequirementRole = String
  object NurseRequirementRole:
    @targetName("fromNurseRequirementRole")
    def from(s: String): Result[NurseRequirementRole] =
      if (s == null) Left(DomainError.NurseRequirementRoleCannotBeNull("Nurse's requirement role can not be null"))
      else Right(s)
  extension (n: NurseRequirementRole)
    @targetName("toNurseRequirementRole")
    def to: String = n
  
  // Nurse name attribute
  opaque type NurseName = String
  object NurseName:
    @targetName("fromNurseName")
    def from(s: String): Result[NurseName] =
      if (s == null) Left(DomainError.NurseNameCannotBeNull("Nurse's name can not be null"))
      else Right(s)
  extension (n: NurseName)
    @targetName("toNurseName")
    def to: String = n

  // Period for period preference attribute
  opaque type PeriodType = String
  object PeriodType:
    @targetName("fromPeriod")
    def from(s: String): Result[PeriodType] =
      if (s == null) Left(DomainError.PeriodCannotBeNull("The period can not be null"))
      else Right(s)
  extension (n: PeriodType)
    @targetName("toPeriodType")
    def to: String = n

  opaque type DayType = Int
  object DayType:
    @targetName("fromDayType")
    def from(s: Int): Result[DayType] =
      if (s < 1 || s > 7) Left(DomainError.DayOutOfBounds("The Day must be between 1 and 7"))
      else Right(s)
  extension (n: DayType)
    @targetName("toDayType")
    def to: Int = n

  opaque type PreferenceValue = Int
  object PreferenceValue:
    @targetName("fromPreferenceValue")
    def from(i: Int): Result[PreferenceValue] =
      if (i < -2 || i > 2) Left(DomainError.PreferenceOutOfBounds("Preference value can't be less than -2 or more than 2"))
      else Right(i)
  extension (p: PreferenceValue)
    @targetName("toPreferenceValue")
    def to: Int = p
    @targetName("<PreferenceValue")
    def <(p2: PreferenceValue): Boolean = p < p2
    @targetName(">PreferenceValue")
    def >(p2: PreferenceValue): Boolean = p > p2

  opaque type NurseSeniority = Int
  object NurseSeniority:
    @targetName("fromNurseSeniority")
    def from(t: Int): Result[NurseSeniority] =
      if(t < 1 || t > 5) Left(DomainError.NurseSeniorityOutOfBounds("Nurse's seniority must be between 1 and 5"))
      else Right(t)
  extension (s: NurseSeniority) 
    @targetName("toNurseSeniority")
    def to: Int = s

  