package domain.schedule

import SimpleTypes.{NurseName, NurseRole, NurseSeniority, *}

//type Result[A] = Either[Nurse,A]

object DomainObjects:
  final case class ScheduleInfo(schedulePeriods: List[SchedulePeriods], nurses: List[Nurse], constraints: List[Constraint], preferences: List[Preference])
  final case class SchedulePeriods(schedulePeriods: List[SchedulePeriod])
  final case class SchedulePeriod(nurseRequirements: NurseRequirement, schedulePeriodID: SchedulePeriodID, schedulePeriodStart: SchedulePeriodStart, schedulePeriodEnd: SchedulePeriodEnd)
  final case class NurseRequirement(nurseRequirementRole: NurseRequirementRole, nurseRequirementNumber: NurseRequirementNumber)
  final case class Nurse(name: NurseName, seniority: NurseSeniority , role: NurseRole):
    def toXML() =
      <nurse name="{NurseName}" role="{NurseRole}"/>
  final case class Constraint(minRestDaysPerWeek: MinRestDaysPerWeek, maxShiftsPerDay: MaxShiftsPerDay)
  final case class PeriodPreference(nurse: NurseName, period: PeriodType, preferenceValue: PreferenceValue)
  final case class Period(periodPreferences: List[PeriodPreference])
  final case class DayPreference(nurse: NurseName, day: DayType, preferenceValue: PreferenceValue)
  final case class Day(dayPreferences: List[DayPreference])
  final case class Preference(period: List[Period], day: List[Day])