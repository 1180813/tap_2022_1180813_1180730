package domain

type Result[A] = Either[DomainError,A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case MinRestDaysPerWeek(error: String)
  case NurseRequirementNumber(error: String)
  case MaxShiftsPerDayError(error: String)
  case NurseNameCannotBeNull(error: String)
  case NurseRequirementRoleCannotBeNull(error: String)
  case NurseSeniorityOutOfBounds(error: String)
  case NurseRoleCannotBeNull(error: String)
  case PeriodCannotBeNull(error: String)
  case DayOutOfBounds(error: String)
  case PreferenceOutOfBounds(error: String)
  case SchedulePeriodID(error: String)
  case SchedulePeriodEndCannotBeNull(error: String)
  case SchedulePeriodStartCannotBeNull(error: String)

  